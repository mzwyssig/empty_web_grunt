# Empty web folder - grub managed

You need npm to be installed on your machine.

`# apt-get update && apt-get install npm` (Debian, Ubuntu) or `# pacman -Syu npm` (Archlinux)

## To use this ##
1. Clone the repository
2. Get into the theme directory `cd resources/themes`
3. Install the proper dependencies (will create a **node_modules** folder) with `npm install`.
4. Launch the compilation of the files into the public/ folder with `grunt`.

The _grunt_ configuration is set in **Gruntfile.js**.

The _npm_ configuration is set in **package.json**
