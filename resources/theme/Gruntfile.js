module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          outputStyle: 'compressed',
          sourceMap: true,
        },
        files: {
          '../../public/assets/css/app.css': './assets/stylesheets/app.scss'
        }
      }
    },
    concat: {
      options: {
        separator: ';',
      },
      js_frontend: {
        src: [
          './node_modules/angular/angular.min.js',
          './node_modules/angular-route/angular-route.min.js',
          './assets/javascript/app.js'
        ],
        dest: '../../public/assets/js/app.js',
      }
    },
    uglify: {
      options: {
        mangle: false  // Use if you want the names of your functions and variables unchanged
      },
      frontend: {
        files: {
          '../../public/assets/js/app.js': '../../public/assets/js/app.js',
        }
      }
    },
    watch: {
      grunt: {
        options: {
          reload: true
        },
        files: ['Gruntfile.js']
      },

      js_frontend: {
        files: [
          //watched files
          './node_modules/angular/angular.js',
          './node_modules/angular/angular-route.js',
          './assets/javascript/app.js'
        ],
        tasks: ['concat:js_frontend','uglify:frontend'],     //tasks to run
        options: {
          livereload: true                        //reloads the browser
        }
      },
      sass: {
        files: './assets/stylesheets/**/*.scss',
        tasks: ['sass'],
	options: {
	  livereload: true
	}
      }

    }
  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.loadNpmTasks('grunt-sass');


  grunt.registerTask('build', ['sass']);
  grunt.registerTask('default', ['build','watch']);
}
